#include <stdio.h>
#include <stdbool.h>
//#include "stringsort.h"

#include "tft.h"
//
//

int getNum(char* string){
	bool number_found = false;
	bool number_finished = false;
	int size = 0;
	int factor = 1;
	int number = 0;
	
	char c = 'x';
	while (c != '\0') {
		c = string[size++];
	}
	
	int i = size - 2;
	while (i > 0 && !(number_found && number_finished)) {
		if ('0' <= string[i] && string[i] <= '9') {
			number_found = true;
			int temp_number = (int) string[i] - '0'; // test this
			temp_number *= factor;
			factor *= 10;
			number += temp_number;
		} else {
			if (number_found) {
				number_finished = true;
			}
		}
		i--;
	}
	return number;
}
//
void SortiereStrings(char** list, int size){
	int list_numbers[size];
	char* temp_char;
	
	for (int i = 0; i < size; i++) {
		list_numbers[i] = getNum(list[i]);
	}
	
	bool is_sorted = false;
	
	while (!is_sorted) {
		is_sorted = true;
		for (int i = 0; i < size - 1; i++) {
			if (list_numbers[i+1] < list_numbers[i]) {
				int temp_num = list_numbers[i+1];
				list_numbers[i+1] = list_numbers[i];
				list_numbers[i] = temp_num;
				
				temp_char = list[i+1];
				list[i+1] = list[i];
				list[i] = temp_char;
				
				is_sorted = false;
			}
		}
	}
}
	
void PrintStringliste(char** list, int size) {
    printf("Adresse von der Liste: %p", &list);
	for (int i = 0; i < size; i++) {
        printf("Adresse vom String: %p ", &list[i]);
		TFT_puts(list[i]);
		TFT_newline();
		TFT_carriage_return();
	}
}
// 

