#include <stdint.h>

#ifndef stringsort
#define stringsort
//#include <>

char *pPro100Euro[] = 	{
     "D�n. Krone      745 ",
		 "US Dollar       116 ",
     "Schw. Franken   115 ",
	   "Brit. Pfund      87 ",
     "T�rk. Lira      549 ",
     //""		// soll als Feldendekennzeichen genutzt werden
     };
// Falls keine andere L�sung verf�gbar, dann mit numerischer Anzahl der Elemente
uint8_t N_STRING = (uint8_t)(sizeof(pPro100Euro)/sizeof(pPro100Euro[0]));
//uint8_t N_STRING = 5;

void SortiereStrings(char** list, int size);
void PrintStringliste(char** list, int size);
int getNum(char* string);
#endif
