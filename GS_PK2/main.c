/**
  ******************************************************************************
  * @file    	main.c 
  * @author  	B. schwarz
  *        	  HAW-Hamburg
  *          	Labor f�r technische Informatik
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    22.06.2018
  * @brief   Main program body
	*					 GS Probe-Klausur 28.06.2018
	*					 Aufgabe 5: Sortierung von Strings nach Inhalt
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>

#include "TI_Lib.h"
#include "tft.h"

#define IDR_MASK_PIN_4        (0x01U << 4)	// 

//--- For GPIOs -----------------------------
//Include instead of "stm32f4xx.h" for
//compatibility between Simulation and Board
#include "TI_memory_map.h"
//#include "stm32f4xx.h"
//--- For Touch Keypad ----------------------
//#include "keypad.h"

//--- For Timer -----------------------------
//#include "timer.c"
#include "stringsort.h"
/**
  * @brief  Main program
  * @param  None
  */
int main(void)
{
	uint8_t Pin_State = IDR_MASK_PIN_4 != (GPIOE -> IDR & IDR_MASK_PIN_4);
  Init_TI_Board(); // Name in String eintragen
  printf("GS Probe-Klausur 28.06.2018\n\r");
  TFT_cls();
  TFT_puts("GS Probe-Klausr 28.06.2018\n\r");
	                 ;
	//
	
	while(Pin_State         ){
		Pin_State = IDR_MASK_PIN_4 != (GPIOE -> IDR & IDR_MASK_PIN_4);
		PrintStringliste(pPro100Euro, N_STRING);	// vorher
	}
		TFT_newline();
	  TFT_carriage_return();
		SortiereStrings(pPro100Euro, N_STRING); // ruft getNum() auf;
		PrintStringliste(pPro100Euro, N_STRING); // nachher

  return 0;

}

/*int main(void) {
	Init_TI_Board();
	printf("\n%d", getNum("ABC 512"));
}*/
// EOF
