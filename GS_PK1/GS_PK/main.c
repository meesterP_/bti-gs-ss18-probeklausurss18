/**
  ******************************************************************************
  * @file    	main.c Rumpf
  * @author  	B.Schwarz
  *        	  HAW-Hamburg
  *          	Labor f�r technische Informatik
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    21.06.2018
	*	@				 GS Probeklausur f�r B-TI2 Aufgabe 2 bis 4
	* @				 28.06.2018
  * @brief   
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
// Verwendung f�r welche Fkt. mit Kommentar angeben! 
#include <stdio.h>		
#include <stdint.h>	// ?
#include <string.h>	// strcyp
#include "TI_Lib.h"
#include "tft.h"

//--- For GPIOs -----------------------------
//Include instead of "stm32f4xx.h" for
//compatibility between Simulation and Board
//#include "TI_memory_map.h"
#include "stm32f4xx.h"	// TI-Board GPIO access
//--- For Touch Keypad ----------------------
//#include "keypad.h"

//--- For Timer -----------------------------
//#include "timer.c"


// Code_3
#define IDR_MASK_PIN_4        (0x01U << 4)	// 
uint8_t gpioePin4State;
// Code_4
#define OTYPER_MASK_PIN_5			(0x01U << 5)	// Code_4 
#define PUPDR_MASK_PU(PIN) 		(0x01U<< 2*(PIN))// pull up
// Code_2_1
int16_t *ptr_i[3];
int16_t a=20, b=30, c=40; // Code_2_1 
int main(void)
{
	int16_t aa=32767, bb=30, cc=40; // Code_2_1 
  Init_TI_Board();
  printf("GS Probe-Klausur 28.06.18\n\r");
  TFT_cls();
  TFT_puts("GS Probe-Klausur 28.06.18 \n\r");
	// Code_2_1
	ptr_i[0] = &aa;
	ptr_i[1] = &b;
	ptr_i[2] = &c;
	
	// Code_2_2
	char AIRP[]="Transatlantik";
  char *JET[]= {"AB_320","BO_737","DC_54"};
	
	//strcpy(AIRP, JET[0]);
	char* AIRP_mod = AIRP;
	printf("AIRP neu %s, %p\n", AIRP_mod, AIRP_mod);
	
	// Code_3
	uint8_t gpioePin4State = IDR_MASK_PIN_4 != (GPIOE -> IDR & IDR_MASK_PIN_4);
	
	// Code_4 Read modify Write
	GPIOG->OTYPER |= OTYPER_MASK_PIN_5;
	GPIOG->PUPDR &= ~(0x3 << 2*5);
	GPIOG->PUPDR |= PUPDR_MASK_PU(5);
	while(1) {
		gpioePin4State = IDR_MASK_PIN_4 != (GPIOE -> IDR & IDR_MASK_PIN_4);
		GPIOG->BSRRH = (0x01U << (5));
		
		if (gpioePin4State) {
			GPIOG->BSRRL = (0x01U << (5));
		}
	}
	
  return 0;

}
// EOF
